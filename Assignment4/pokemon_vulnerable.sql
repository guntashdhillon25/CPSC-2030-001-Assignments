-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:08 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_vulnerable`
--

CREATE TABLE `pokemon_vulnerable` (
  `type` char(255) DEFAULT NULL,
  `vulnerable_to` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon_vulnerable`
--

INSERT INTO `pokemon_vulnerable` (`type`, `vulnerable_to`) VALUES
('normal', 'fight'),
('fight', 'flying'),
('fight', 'psychc'),
('fight', 'fairy'),
('flying', 'rock'),
('flying', 'electc'),
('flying', 'ice'),
('poison', 'ground'),
('poison', 'psychc'),
('ground', 'water'),
('ground', 'grass'),
('ground', 'ice'),
('rock', 'fight'),
('rock', 'ground'),
('rock', 'steel'),
('rock', 'water'),
('rock', 'grass'),
('bug', 'flying'),
('bug', 'rock'),
('bug', 'fire'),
('ghost', 'ghost'),
('ghost', 'dark'),
('steel', 'fight'),
('steel', 'ground'),
('steel', 'fire'),
('fire', 'ground'),
('fire', 'rock'),
('fire', 'water'),
('water', 'grass'),
('water', 'electc'),
('grass', 'flying'),
('grass', 'posion'),
('grass', 'bug'),
('grass', 'fire'),
('grass', 'ice'),
('electc', 'ground'),
('psychc', 'bug'),
('psychc', 'ghost'),
('psychc', 'dark'),
('ice', 'fight'),
('ice', 'rock'),
('ice', 'steel'),
('ice', 'fire'),
('dragon', 'ice'),
('dragon', 'dragon'),
('dragon', 'fairy'),
('fairy', 'poison'),
('fairy', 'steel'),
('dark', 'fight'),
('dark', 'bug'),
('dark', 'fairy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon_vulnerable`
--
ALTER TABLE `pokemon_vulnerable`
  ADD KEY `type` (`type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokemon_vulnerable`
--
ALTER TABLE `pokemon_vulnerable`
  ADD CONSTRAINT `pokemon_vulnerable_ibfk_1` FOREIGN KEY (`type`) REFERENCES `pokemon_types` (`type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:08 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_weak`
--

CREATE TABLE `pokemon_weak` (
  `type` char(255) DEFAULT NULL,
  `weak_against` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon_weak`
--

INSERT INTO `pokemon_weak` (`type`, `weak_against`) VALUES
('normal', 'rock'),
('normal', 'ghost'),
('normal', 'steel'),
('fight', 'flying'),
('fight', 'poison'),
('fight', 'psychc'),
('fight', 'bug'),
('fight', 'ghost'),
('fight', 'fairy'),
('flying', 'rock'),
('flying', 'steel'),
('flying', 'electc'),
('poison', 'poison'),
('poison', 'ground'),
('poison', 'rock'),
('poison', 'ghost'),
('poison', 'steel'),
('ground', 'flying'),
('ground', 'bug'),
('ground', 'grass'),
('rock', 'fight'),
('rock', 'ground'),
('rock', 'steel'),
('bug', 'fight'),
('bug', 'flying'),
('bug', 'poison'),
('bug', 'ghost'),
('bug', 'steel'),
('bug', 'fire'),
('bug', 'fairy'),
('ghost', 'normal'),
('ghost', 'dark'),
('steel', 'steel'),
('steel', 'fire'),
('steel', 'water'),
('steel', 'electc'),
('fire', 'rock'),
('fire', 'fire'),
('fire', 'water'),
('fire', 'dragon'),
('water', 'water'),
('water', 'grass'),
('water', 'dragon'),
('grass', 'flying'),
('grass', 'poison'),
('grass', 'bug'),
('grass', 'steel'),
('grass', 'fire'),
('grass', 'grass'),
('grass', 'dragon'),
('electc', 'ground'),
('electc', 'grass'),
('electc', 'electc'),
('electc', 'dragon'),
('psychc', 'steel'),
('psychc', 'pcychc'),
('psychc', 'dark'),
('ice', 'steel'),
('ice', 'fire'),
('ice', 'water'),
('ice', 'ice'),
('dragon', 'steel'),
('dragon', 'fairy'),
('fairy', 'poison'),
('fairy', 'steel'),
('fairy', 'fire'),
('dark', 'fight'),
('dark', 'dark'),
('dark', 'fairy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon_weak`
--
ALTER TABLE `pokemon_weak`
  ADD KEY `type` (`type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokemon_weak`
--
ALTER TABLE `pokemon_weak`
  ADD CONSTRAINT `pokemon_weak_ibfk_1` FOREIGN KEY (`type`) REFERENCES `pokemon_types` (`type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

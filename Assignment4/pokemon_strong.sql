-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:07 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_strong`
--

CREATE TABLE `pokemon_strong` (
  `type` char(255) DEFAULT NULL,
  `strong_against` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon_strong`
--

INSERT INTO `pokemon_strong` (`type`, `strong_against`) VALUES
('normal', 'null'),
('fight', 'normal'),
('fight', 'rock'),
('fight', 'steel'),
('fight', 'ice'),
('fight', 'dark'),
('flying', 'fight'),
('flying', 'bug'),
('flying', 'grass'),
('poison', 'grass'),
('poison', 'fairy'),
('ground', 'poison'),
('ground', 'rock'),
('ground', 'steel'),
('ground', 'fire'),
('ground', 'electc'),
('rock', 'flying'),
('rock', 'bug'),
('rock', 'fire'),
('rock', 'ice'),
('bug', 'grass'),
('bug', 'psychc'),
('bug', 'dark'),
('ghost', 'ghost'),
('ghost', 'psychc'),
('steel', 'rock'),
('steel', 'ice'),
('steel', 'fairy'),
('fire', 'bug'),
('fire', 'steel'),
('fire', 'grass'),
('fire', 'ice'),
('water', 'ground'),
('water', 'rock'),
('water', 'fire'),
('grass', 'ground'),
('grass', 'rock'),
('grass', 'water'),
('electc', 'flying'),
('electc', 'water'),
('psychc', 'fight'),
('psychc', 'poison'),
('ice', 'flying'),
('ice', 'ground'),
('ice', 'grass'),
('ice', 'dragon'),
('dragon', 'dragon'),
('fairy', 'fight'),
('fairy', 'dragon'),
('fairy', 'dark'),
('dark', 'ghost'),
('dark', 'psychc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon_strong`
--
ALTER TABLE `pokemon_strong`
  ADD KEY `type` (`type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokemon_strong`
--
ALTER TABLE `pokemon_strong`
  ADD CONSTRAINT `pokemon_strong_ibfk_1` FOREIGN KEY (`type`) REFERENCES `pokemon_types` (`type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

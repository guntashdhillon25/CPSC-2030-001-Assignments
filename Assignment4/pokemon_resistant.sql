-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:07 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_resistant`
--

CREATE TABLE `pokemon_resistant` (
  `type` char(255) DEFAULT NULL,
  `resistance_to` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon_resistant`
--

INSERT INTO `pokemon_resistant` (`type`, `resistance_to`) VALUES
('normal', 'ghost'),
('fight', 'rock'),
('fight', 'bug'),
('fight', 'dark'),
('flying', 'fight'),
('flying', 'ground'),
('flying', 'bug'),
('flying', 'grass'),
('poison', 'fight'),
('poison', 'posion'),
('poison', 'grass'),
('poison', 'fairy'),
('ground', 'posion'),
('ground', 'rock'),
('ground', 'electc'),
('rock', 'normal'),
('rock', 'flying'),
('rock', 'posion'),
('rock', 'fire'),
('bug', 'fight'),
('bug', 'ground'),
('bug', 'grass'),
('ghost', 'normal'),
('ghost', 'fight'),
('ghost', 'poison'),
('ghost', 'bug'),
('steel', 'normal'),
('steel', 'flying'),
('steel', 'posion'),
('steel', 'rock'),
('steel', 'bug'),
('steel', 'steel'),
('steel', 'grass'),
('steel', 'psychc'),
('steel', 'ice'),
('steel', 'dragon'),
('steel', 'fairy'),
('fire', 'bug'),
('fire', 'steel'),
('fire', 'fire'),
('fire', 'grass'),
('fire', 'ice'),
('water', 'steel'),
('water', 'fire'),
('water', 'water'),
('water', 'ice'),
('grass', 'ground'),
('grass', 'water'),
('grass', 'grass'),
('grass', 'electc'),
('electc', 'flying'),
('electc', 'steel'),
('electc', 'electc'),
('psychc', 'fight'),
('psychc', 'psychc'),
('ice', 'ice'),
('dragon', 'fire'),
('dragon', 'water'),
('dragon', 'grass'),
('dragon', 'electc'),
('fairy', 'fight'),
('fairy', 'bug'),
('fairy', 'dragon'),
('fairy', 'dark'),
('dark', 'ghost'),
('dark', 'psychc'),
('dark', 'dark');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon_resistant`
--
ALTER TABLE `pokemon_resistant`
  ADD KEY `type` (`type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokemon_resistant`
--
ALTER TABLE `pokemon_resistant`
  ADD CONSTRAINT `pokemon_resistant_ibfk_1` FOREIGN KEY (`type`) REFERENCES `pokemon_types` (`type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 07:07 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon_hoenn`
--

CREATE TABLE `pokemon_hoenn` (
  `hoenn` int(11) DEFAULT NULL,
  `name` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon_hoenn`
--

INSERT INTO `pokemon_hoenn` (`hoenn`, `name`) VALUES
(97, 'Budew'),
(99, 'Roserade'),
(157, 'Chingling'),
(86, 'Magnezone'),
(178, 'Rhyperior'),
(32, 'Gallade'),
(32, 'Mega Gallade'),
(62, 'Probopass'),
(155, 'Dusknoir'),
(181, 'Froslass');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemon_hoenn`
--
ALTER TABLE `pokemon_hoenn`
  ADD KEY `name` (`name`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokemon_hoenn`
--
ALTER TABLE `pokemon_hoenn`
  ADD CONSTRAINT `pokemon_hoenn_ibfk_1` FOREIGN KEY (`name`) REFERENCES `pokemon` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `name`()
    NO SQL
SELECT distinct pokemon.name,pokemon_type.type,pokemon.Nat
from pokemon_type,pokemon
where pokemon.name=pokemon_type.name$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `poke_name`(IN `txt` VARCHAR(255))
    NO SQL
select pokemon.*
from pokemon 
where name=txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `type`(IN `txt` VARCHAR(255))
    NO SQL
SELECT distinct pokemon.name,pokemon_type.type,pokemon.Nat
from pokemon_type,pokemon
where pokemon_type.type=txt$$
DELIMITER ;



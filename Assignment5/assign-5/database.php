<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="style.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Document</title>
</head>

<?php
//Setup
//database stuff first
   //SQL  portion
   $user = 'CPSC2030';
   $pwd = 'CPSC2030';
   $server = 'localhost';
   $dbname = 'pokedex';

   $conn = new mysqli($server, $user, $pwd, $dbname);
  
   if(isset($_GET['type'])) {
    $type = mysqli_real_escape_string($conn, $_GET["type"]);
    $result = $conn->query("call type(\"$type\")");  
   }
   else{
    $result = $conn->query("call name()");
   }
?>
<body>
<a href="database.php">Previous Page</a><br>
<h3>Please choose a type:</h3> 
<br>
<?php 
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if (($result)->num_rows > 0) {
    while($row = mysqli_fetch_assoc($result)) {
     echo  "<div>" . $row["Nat"] . "<br>"."<a href= 'database2.php?names=".$row['name']." '>".$row["name"] . "</a>". "</br>" ."<a href= 'database.php?type=".$row['type']." '>".$row["type"] . "</a>". "</div>"   ;
    }
}
 else {
    echo "0 results";
}
?>
</body>
</html>